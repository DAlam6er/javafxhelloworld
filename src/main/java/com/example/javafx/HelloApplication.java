package com.example.javafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class HelloApplication extends Application
{
    @Override
    public void start(Stage primaryStage)
    {
        Button btn = new Button("Say 'Hello'");
        Label lbl = new Label("Input your name:");
        Label helloLbl = new Label();
        TextField txt = new TextField();

        btn.setOnAction((ActionEvent event) -> {
            if ("".equals(txt.getText())) {
                helloLbl.setText("Hello World!");
            } else {
                helloLbl.setText("Hello " + txt.getText() + "!");
            }
        });
        GridPane root = new GridPane();
        // обращаемся как к таблице, т.к. добавляем в сам GridPane.
        // 0,0 - элементы матрицы (столбец, ряд)
        root.add(lbl, 0, 0);
        root.add(txt, 1, 0);
        root.add(btn, 1, 1);
        root.add(helloLbl, 1, 2);

//        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));

        Scene scene = new Scene(root, 300, 250);
        primaryStage.setTitle("Hello!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args)
    {
        launch();
    }
}